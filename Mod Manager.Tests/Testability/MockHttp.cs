﻿using Avalonia.Media.Imaging;
using Mod_Manager.Abstractions;

namespace Mod_Manager.Tests.Testability;

sealed class MockHttp : IHttp
{
    public Task<Bitmap?> GetImageAsync(string url)
    {
        throw new NotImplementedException();
    }

    public Task<Stream?> GetImageStreamAsync(string url)
    {
        throw new NotImplementedException();
    }

    public void OpenUrlFromSteam(string url)
    {
        throw new NotImplementedException();
    }

    public void OpenUrl(string url)
    {
        throw new NotImplementedException();
    }

    public void OpenInSteam(string url)
    {
        throw new NotImplementedException();
    }
}