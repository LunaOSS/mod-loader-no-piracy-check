﻿using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.Steamworks;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.ViewModels;

public class ItemView
{
    [Test]
    public void displays_subscribe_text()
    {
        var fakePublishedField = new Mock<IPublishedFieldId>();
        fakePublishedField.Setup(p => p.Value).Returns(1);
        
        var fakeSteamItem = new Mock<IItem>();
        fakeSteamItem.Setup(i => i.Id).Returns(fakePublishedField.Object);
        fakeSteamItem.Setup(i => i.IsSubscribed).Returns(false);

        var fakeNotification = new Mock<INotification>();
        
        var fakeItem = new Mock<Item>(fakeSteamItem.Object);
        //var model = new ItemViewModel(fakeItem.Object, fakeNotification.Object, Mock.Of<IHttp>(), new List<IBBTag>());

        //model.SubButtonText.Should().Be("Subscribe");
    }
    
    [Test]
    public void displays_subscribed_text()
    {
        var fakePublishedField = new Mock<IPublishedFieldId>();
        fakePublishedField.Setup(p => p.Value).Returns(1);
        
        var fakeSteamItem = new Mock<IItem>();
        fakeSteamItem.Setup(i => i.Id).Returns(fakePublishedField.Object);
        fakeSteamItem.Setup(i => i.IsSubscribed).Returns(true);
        
        var fakeItem = new Mock<Item>(fakeSteamItem.Object);
        var fakeNotification = new Mock<INotification>();
        //var model = new ItemViewModel(fakeItem.Object, fakeNotification.Object, Mock.Of<IHttp>(), new List<IBBTag>());

        //model.SubButtonText.Should().Be("Subscribed");
    }
}