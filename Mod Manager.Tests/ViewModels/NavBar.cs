﻿using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.Steamworks;

namespace Mod_Manager.Tests.ViewModels;

public class NavBar
{
    public class Can_open
    {
        [Test]
        public void Home()
        {
            var fakeQuery = new Mock<IQuery>();
            //var mainWindow = new MainWindowViewModel(fakeQuery.Object, null, Mock.Of<IHttp>(), Enumerable.Empty<IBBTag>());
            
            //mainWindow.HeaderBarView.HomeButtonCommand.Execute().Subscribe();
            //mainWindow.MainView.Should().BeOfType<HomeViewModel>();
        }
        
        [Test]
        public void Settings()
        {
            var fakeQuery = new Mock<IQuery>();
            //var mainWindow = new MainWindowViewModel(fakeQuery.Object, null, Mock.Of<IHttp>(), Enumerable.Empty<IBBTag>());
            
            //mainWindow.HeaderBarView.SettingsButtonCommand.Execute().Subscribe();
            //mainWindow.MainView.Should().BeOfType<SettingsViewModel>();
        }
    }

    [Test]
    [TestCase(FilesState.Modded, "Disable")]
    [TestCase(FilesState.Vanilla, "Enable")]
    [TestCase(FilesState.Unknown, "Unknown")]
    public void toggle_button_is_visually_correct(FilesState filesState, string expectedText)
    {
        var fakeQuery = new Mock<IQuery>();
        var instance = new Mock<IInit>();
        instance.Setup(init => init.GetCurrentState()).Returns(filesState);
        //var mainWindow = new MainWindowViewModel(fakeQuery.Object, instance.Object, Mock.Of<IHttp>(), Enumerable.Empty<IBBTag>());
        //var navView = mainWindow.HeaderBarView;
        //navView.UpdateToggleButton();
        //navView.ToggleButtonText.Should().Be(expectedText);

        /*switch (filesState)
        {
            case FilesState.Unknown:
                navView.ToggleIsWarningColour.Should().BeTrue();
                navView.ToggleIsSuccessColour.Should().BeFalse();
                navView.ToggleIsDangerColour.Should().BeFalse();
                break;
            case FilesState.Modded:
                navView.ToggleIsDangerColour.Should().BeTrue();
                navView.ToggleIsWarningColour.Should().BeFalse();
                navView.ToggleIsSuccessColour.Should().BeFalse();
                break;
            case FilesState.Vanilla:
                navView.ToggleIsSuccessColour.Should().BeTrue();
                navView.ToggleIsWarningColour.Should().BeFalse();
                navView.ToggleIsDangerColour.Should().BeFalse();
                break; 
        }*/
    }
}