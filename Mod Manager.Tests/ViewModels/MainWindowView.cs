﻿using Mod_Manager.Abstractions.Steamworks;

namespace Mod_Manager.Tests.ViewModels;

public class MainWindowView
{
    class when_opening_workshop_item
    {
        [Test]
        public async Task main_view_is_changed()
        {
            const ulong steamID = 123;
            
            var item = new Mock<IItem>();
            item.Setup(i => i.Id.Value).Returns(steamID);
            var fakeResults = new Mock<IResultPage>();
            fakeResults.Setup(r => r.Entries).Returns(new List<IItem> { item.Object });
            var queryMock = new Mock<IQuery>();
            queryMock.Setup(q => q.WithFileId(It.IsAny<ulong>())).Returns(queryMock.Object);
            queryMock.Setup(q => q.WithLongDescription(It.IsAny<bool>())).Returns(queryMock.Object);
            queryMock.Setup(q => q.GetPageAsync(It.IsAny<int>())).Returns(Task.FromResult<IResultPage?>(fakeResults.Object));

            //var mainWindow = new MainWindowViewModel(queryMock.Object, null, Mock.Of<IHttp>(), Enumerable.Empty<IBBTag>());
            //await mainWindow.OpenWorkshopItem(queryMock.Object, steamID);
            //mainWindow.MainView.Should().BeOfType<ItemViewModel>();
        }

        [Test]
        public async Task main_view_is_home_page_if_id_is_invalid()
        {
            const ulong invalidSteamId = 123;
            var queryMock = new Mock<IQuery>();
            queryMock.Setup(q => q.WithFileId(It.IsAny<ulong>())).Returns(queryMock.Object);
            queryMock.Setup(q => q.WithLongDescription(It.IsAny<bool>())).Returns(queryMock.Object);
            queryMock.Setup(q => q.GetPageAsync(It.IsAny<int>())).Returns(Task.FromResult<IResultPage?>(null));

            //var mainWindow = new MainWindowViewModel(queryMock.Object, null, Mock.Of<IHttp>(), Enumerable.Empty<IBBTag>());
            //await mainWindow.OpenWorkshopItem(queryMock.Object, invalidSteamId);
            //mainWindow.MainView.Should().BeOfType<HomeViewModel>();
        }
    }
}