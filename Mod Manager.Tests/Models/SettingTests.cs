﻿using Mod_Manager.Abstractions.Steamworks;

namespace Mod_Manager.Tests.Models;

public class SettingTests
{
    private const string fileName = "settings.json";
    [Test]
    public void can_create_constructor()
    {
        var fakeSteamStorage = new Mock<ISteamRemoteStorage>();
        var settings = new Mod_Manager.Models.Settings(fakeSteamStorage.Object);
    }

    [Test]
    public void can_set_vtol_path_without_prior_settings()
    {
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        var fakeSteamStorage = new Mock<ISteamRemoteStorage>();
        
        fakeSteamStorage
            .Setup(instance => instance.FileExists(fakeVtolDir))
            .Returns(false);
        fakeSteamStorage
            .Setup(instance => instance.FileWrite(fileName, It.IsAny<string>()))
            .Returns(true);
        
        var settings = new Mod_Manager.Models.Settings(fakeSteamStorage.Object);
        settings.SetLastVTOLPath(fakeVtolDir);
    }

    [Test]
    public void can_get_vtol_from_cloud()
    {
        const string fakeVtolDirJson = @"C:\\Program Files (x86)\\Steam\\steamapps\\common\\VTOL VR";
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        const string jsonText = "{ \"vtol_path\": \"" + fakeVtolDirJson + "\" }";
        var fakeSteamStorage = new Mock<ISteamRemoteStorage>();

        fakeSteamStorage
            .Setup(instance => instance.FileExists(fileName))
            .Returns(true);
        fakeSteamStorage
            .Setup(instance => instance.FileReadText(fileName))
            .Returns(jsonText);
        
        var settings = new Mod_Manager.Models.Settings(fakeSteamStorage.Object);
        settings.GetLastVTOLPath().Should().Be(fakeVtolDir);
    }
}