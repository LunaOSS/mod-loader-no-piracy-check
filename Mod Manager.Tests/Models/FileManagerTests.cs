﻿using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using Mod_Manager.Abstractions;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.Models;

public class FileManagerTests
{
    public class can_return_state_when
    {
        [Test]
        public void doorstop_config_is_missing()
        {
            const string currentDir = @"D:\Program Files (x86)\Steam\steamapps\common\VTOL VR\@Mod Loader";
            const string vtolDir = @"D:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { currentDir, new MockDirectoryData() },
                { vtolDir, new MockDirectoryData() }
            });
            fileSystem.Directory.SetCurrentDirectory(currentDir);
            
            var fileManager = new FileManager(fileSystem);
            fileManager.GetCurrentState(vtolDir).Should().Be(FilesState.Vanilla);
        }
    }

    [Test]
    public void returns_parent_directory_when_getting_vtol_directory()
    {
        const string currentDir = @"D:\Program Files (x86)\Steam\steamapps\common\VTOL VR\@Mod Loader";
        const string vtolDir = @"D:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            
        var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
        {
            { currentDir, new MockDirectoryData() },
            { vtolDir, new MockDirectoryData() }
        });
        fileSystem.Directory.SetCurrentDirectory(currentDir);
            
        var fileManager = new FileManager(fileSystem);
        fileManager.GetVtolDirectory().Should().BeEquivalentTo(vtolDir);
    }
}