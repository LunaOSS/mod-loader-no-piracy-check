﻿using System.IO.Abstractions.TestingHelpers;
using Mod_Manager.Abstractions;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.Models;

public class InitTests
{
    [Test]
    public void can_find_vtol_in_parent_folder()
    {
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        const string fakeCurrentDir = @$"{fakeVtolDir}\@Mod Loader";
        const string vtolExeName = fakeVtolDir + @"\VTOLVR.exe";
        const string dataFolderName = fakeVtolDir + @"\VTOLVR_Data";
            
        var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
        {
            { vtolExeName, new MockFileData(String.Empty) },
            { dataFolderName, new MockDirectoryData() },
            { fakeCurrentDir, new MockDirectoryData() }
                
        });
        fileSystem.Directory.SetCurrentDirectory(fakeCurrentDir);

        var init = new Init(fileSystem, null, null);
        init.IsInGameFiles().Should().BeTrue();
    }

    [Test]
    public void can_fail_finding_vtol_in_parent_folder()
    {        
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        const string fakeCurrentDir = @$"{fakeVtolDir}\@Mod Loader";
        const string dataFolderName = fakeVtolDir + @"\VTOLVR_Data";
        const string saveDataFolderName = fakeVtolDir + @"\SaveData";
        const string radioMusicFolderName = fakeVtolDir + @"\RadioMusic";
            
        var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
        {
            // Missing "VTOL VR.exe"
            { dataFolderName, new MockDirectoryData() },
            { fakeCurrentDir, new MockDirectoryData() },
            { saveDataFolderName, new MockDirectoryData() },
            { radioMusicFolderName, new MockDirectoryData() },
                
        });
        fileSystem.Directory.SetCurrentDirectory(fakeCurrentDir);

        var init = new Init(fileSystem, null, null);
        init.IsInGameFiles().Should().BeFalse(); 
    }

    [Test]
    public void can_fail_finding_vtol_not_in_same_drive()
    {
        const string fakeCurrentDir = @"D:\Program Files (x86)\Steam\steamapps\common\VTOL VR\@Mod Loader";          
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        const string dataFolderName = fakeVtolDir + @"\VTOLVR_Data";
        const string vtolExeName = fakeVtolDir + @"\VTOLVR.exe";
        const string saveDataFolderName = fakeVtolDir + @"\SaveData";
        const string radioMusicFolderName = fakeVtolDir + @"\RadioMusic";
            
        var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
        {
            { dataFolderName, new MockDirectoryData() },
            { vtolExeName, new MockFileData(String.Empty) },
            { fakeCurrentDir, new MockDirectoryData() },
            { saveDataFolderName, new MockDirectoryData() },
            { radioMusicFolderName, new MockDirectoryData() },
                
        });
        fileSystem.Directory.SetCurrentDirectory(fakeCurrentDir);
        
        var init = new Init(fileSystem, null, null);
        init.IsInGameFiles().Should().BeFalse(); 
    }

    class when_enabling_mod_loader
    {
        [Test]
        public void throw_if_current_state_is_unknown()
        {
            var init = new Init(null, null, null);
            var action = () => init.EnableModLoader();            
            init.SetCurrentState(FilesState.Unknown);
            action.Should().ThrowExactlyAsync<Exception>("Verify Files before enabling the Mod Loader");
        }

        [Test]
        public void current_state_changes_to_modded()
        {
            var init = new Init(null, null, Mock.Of<IDoorstopManager>());
            init.SetCurrentState(FilesState.Vanilla);
            init.EnableModLoader();
            init.GetCurrentState().Should().Be(FilesState.Modded);
        }
        
        [Test]
        public void doorstop_enabled_is_called()
        {
            var doorstopManager = new Mock<IDoorstopManager>();
            var init = new Init(null, null, doorstopManager.Object);
            init.SetCurrentState(FilesState.Vanilla);
            init.EnableModLoader();
            
            doorstopManager.Verify(dm => dm.Enable());
        }
    }

    class when_disabling_mod_loader
    {
        [Test]
        public void throw_if_current_state_is_unknown()
        {
            var init = new Init(null, null, null);
            var action = () => init.DisableModLoader();            
            init.SetCurrentState(FilesState.Unknown);
            action.Should().ThrowExactly<Exception>("Verify Files before enabling the Mod Loader");
        }

        [Test]
        public void doorstop_disabled_is_called()
        {
            var doorstopManager = new Mock<IDoorstopManager>();
            var init = new Init(null, null, doorstopManager.Object);
            init.SetCurrentState(FilesState.Modded);
            init.DisableModLoader();
            
            doorstopManager.Verify(dm => dm.Disable());
        }

        [Test]
        public void current_state_is_vanilla()
        {
            var init = new Init(null, null, Mock.Of<IDoorstopManager>());
            init.SetCurrentState(FilesState.Modded);
            init.DisableModLoader();

            init.GetCurrentState().Should().Be(FilesState.Vanilla);
        }
    }
}