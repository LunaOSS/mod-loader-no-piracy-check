﻿using System.IO.Abstractions.TestingHelpers;
using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.ConfigParser;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.Models;

public class DoorstopManagerTests
{
    class on_enabling
    {
        [Test]
        public void removes_an_old_doorstop_config()
        {
            const string doorstopConfigPath =
                @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR\doorstop_config.ini";
            const string vtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
            {
                { doorstopConfigPath, new MockFileData(OldConfigFileContents) }
            });
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(vtolDir);
            
            var configParser = new Mock<IConfigParser>();
            configParser.Setup(cp => cp.SectionExists("UnityDoorstop")).Returns(true);
            
            var doorstopManager = new DoorstopManager(fileSystem, fileManager.Object, configParser.Object);
            doorstopManager.Enable();
            var fileContents = fileSystem.File.ReadAllText(doorstopConfigPath);
            fileContents.Should().Be(DefaultConfigContents);
        }

        [Test]
        public void creates_doorstop_file_if_non_exsits()
        {
            const string doorstopConfigPath =
                @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR\doorstop_config.ini";
            const string vtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
            {
                {vtolDir, new MockDirectoryData()}
            });
            
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(vtolDir);
            
            var doorstopManager = new DoorstopManager(fileSystem, fileManager.Object, new Mock<IConfigParser>().Object);
            doorstopManager.Enable();

            fileSystem.FileExists(doorstopConfigPath).Should().BeTrue();
        }

        [Test]
        public void calls_config_parser_save()
        {
            const string vtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
            {
                {vtolDir, new MockDirectoryData()}
            });
            
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(vtolDir);

            var configParser = new Mock<IConfigParser>();
            var doorstopManager = new DoorstopManager(fileSystem, fileManager.Object, configParser.Object);
            doorstopManager.Enable();

            configParser.Verify(cp => cp.SetValue("General", "enabled", true));
            configParser.Verify(cp => cp.Save());
        }
    }
    
    class on_disabling
    {
        [Test]
        public void removes_an_old_doorstop_config()
        {
            const string doorstopConfigPath =
                @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR\doorstop_config.ini";
            const string vtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
            {
                { doorstopConfigPath, new MockFileData(OldConfigFileContents) }
            });
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(vtolDir);
            
            var configParser = new Mock<IConfigParser>();
            configParser.Setup(cp => cp.SectionExists("UnityDoorstop")).Returns(true);
            
            var doorstopManager = new DoorstopManager(fileSystem, fileManager.Object, configParser.Object);
            doorstopManager.Disable();
            var fileContents = fileSystem.File.ReadAllText(doorstopConfigPath);
            fileContents.Should().Be(DefaultConfigContents);
        }

        [Test]
        public void creates_doorstop_file_if_non_exsits()
        {
            const string doorstopConfigPath =
                @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR\doorstop_config.ini";
            const string vtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
            {
                {vtolDir, new MockDirectoryData()}
            });
            
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(vtolDir);
            
            var doorstopManager = new DoorstopManager(fileSystem, fileManager.Object, new Mock<IConfigParser>().Object);
            doorstopManager.Disable();

            fileSystem.FileExists(doorstopConfigPath).Should().BeTrue();
        }
        
        [Test]
        public void calls_config_parser_save()
        {
            const string vtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
            {
                {vtolDir, new MockDirectoryData()}
            });
            
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(vtolDir);

            var configParser = new Mock<IConfigParser>();
            var doorstopManager = new DoorstopManager(fileSystem, fileManager.Object, configParser.Object);
            doorstopManager.Disable();

            configParser.Verify(cp => cp.SetValue("General", "enabled", false));
            configParser.Verify(cp => cp.Save());
        }
    }

    class is_enabled
    {
        [Test]
        public void calls_config_parser_get_value()
        {
            const string doorstopConfigPath =
                @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR\doorstop_config.ini";
            const string vtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";

            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>()
            {
                { doorstopConfigPath, new MockFileData(DefaultConfigContents) }
            });
            var configParser = new Mock<IConfigParser>();
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory()).Returns(vtolDir);
            var doorstopManager =
                new DoorstopManager(fileSystem, fileManager.Object, configParser.Object);
            doorstopManager.IsEnabled();
            configParser.Verify(cp => cp.GetValue("General", "enabled", true));
        }
    }
    
    public const string DefaultConfigContents = @"# General options for Unity Doorstop
[General]

# Enable Doorstop?
enabled=true

# Path to the assembly to load and execute
# NOTE: The entrypoint must be of format `static void Doorstop.Entrypoint.Start()`
target_assembly=@ModLoader\Managed\ModLoader.dll

# If true, Unity's output log is redirected to <current folder>\output_log.txt
redirect_output_log=false

# If enabled, DOORSTOP_DISABLE env var value is ignored
# USE THIS ONLY WHEN ASKED TO OR YOU KNOW WHAT THIS MEANS
ignore_disable_switch=false


# Options specific to running under Unity Mono runtime
[UnityMono]

# Overrides default Mono DLL search path
# Sometimes it is needed to instruct Mono to seek its assemblies from a different path
# (e.g. mscorlib is stripped in original game)
# This option causes Mono to seek mscorlib and core libraries from a different folder before Managed
# Original Managed folder is added as a secondary folder in the search path
dll_search_path_override=@ModLoader\Managed

# If true, Mono debugger server will be enabled
debug_enabled=false

# When debug_enabled is true, specifies the address to use for the debugger server
debug_address=127.0.0.1:10000

# If true and debug_enabled is true, Mono debugger server will suspend the game execution until a debugger is attached
debug_suspend=false

# Options sepcific to running under Il2Cpp runtime
[Il2Cpp]

# Path to coreclr.dll that contains the CoreCLR runtime
coreclr_path=

# Path to the directory containing the managed core libraries for CoreCLR (mscorlib, System, etc.)
corlib_dir=";
    
    public const string OldConfigFileContents = @"[UnityDoorstop]
# Specifies whether assembly executing is enabled
enabled=true
# Specifies the path (absolute, or relative to the game's exe) to the DLL/EXE that should be executed by Doorstop
targetAssembly=Doorstop.dll
# Specifies whether Unity's output log should be redirected to <current folder>\output_log.txt
redirectOutputLog=false
# If enabled, DOORSTOP_DISABLE env var value is ignored
# USE THIS ONLY WHEN ASKED TO OR YOU KNOW WHAT THIS MEANS
ignoreDisableSwitch=false
# Overrides default Mono DLL search path
# Sometimes it is needed to instruct Mono to seek its assemblies from a different path
# (e.g. mscorlib is stripped in original game)
# This option causes Mono to seek mscorlib and core libraries from a different folder before Managed
# Original Managed folder is added as a secondary folder in the search path
dllSearchPathOverride=


# Settings related to bootstrapping a custom Mono runtime
# Do not use this in managed games!
# These options are intended running custom mono in IL2CPP games!
[MonoBackend]
# Path to main mono.dll runtime library
runtimeLib=
# Path to mono config/etc directory
configDir=
# Path to core managed assemblies (mscorlib et al.) directory
corlibDir=";
}