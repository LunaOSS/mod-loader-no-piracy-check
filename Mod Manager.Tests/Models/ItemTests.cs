﻿using Mod_Manager.Abstractions.Steamworks;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.Models;

public class ItemTests
{
    [Test]
    public async Task can_get_subscribed_items()
    {
        var fakePublishedField = new Mock<IPublishedFieldId>();
        fakePublishedField.Setup(p => p.Value).Returns(1);
        
        var fakeItem = new Mock<IItem>();
        fakeItem.Setup(i => i.Id).Returns(fakePublishedField.Object);
        var fakeItems = new List<IItem> { fakeItem.Object };
        
        var resultsCount = 1;
        var fakeResult = new Mock<IResultPage>();
        fakeResult.Setup(r => r.ResultCount).Returns(resultsCount);
        fakeResult.Setup(r => r.Entries).Returns(fakeItems);
        
        var pageNumber = 1;
        
        var fakeQuery = new Mock<IQuery>();
        fakeQuery.Setup(q => q.WhereUserSubscribed()).Returns(fakeQuery.Object);
        fakeQuery.Setup(q => q.WithLongDescription(true)).Returns(fakeQuery.Object);
        fakeQuery.Setup(q => q.GetPageAsync(pageNumber)).Returns(Task.FromResult(fakeResult.Object)!);
        
        var items = new Items(fakeQuery.Object);
        var pageItems = await items.GetSubscribedItemsAsync(pageNumber);

        pageItems.Count().Should().BeGreaterThan(0);
    }
    
    [Test]
    public async Task can_subscribe()
    {
        var fakePublishedField = new Mock<IPublishedFieldId>();
        fakePublishedField.Setup(p => p.Value).Returns(1);
        
        var fakeItem = new Mock<IItem>();
        fakeItem.Setup(i => i.Id).Returns(fakePublishedField.Object);
        fakeItem.Setup(i => i.Unsubscribe()).Returns(Task.FromResult(true)); 
        var item = new Item(fakeItem.Object);
        
        await item.ToggleSubscribedState();
    }
    
    [Test]
    public async Task can_unsubscribe()
    {
        var fakePublishedField = new Mock<IPublishedFieldId>();
        fakePublishedField.Setup(p => p.Value).Returns(1);
        
        var fakeSteamItem = new Mock<IItem>();
        fakeSteamItem.Setup(i => i.Id).Returns(fakePublishedField.Object);
        fakeSteamItem.Setup(i => i.Unsubscribe()).Returns(Task.FromResult(true));
        fakeSteamItem.Setup(i => i.IsSubscribed).Returns(true);
        var item = new Item(fakeSteamItem.Object);
        
        await item.ToggleSubscribedState();
    }
}