﻿using Mod_Manager.Data;

namespace Mod_Manager.Tests.Data;

public class LoadOnStartTests
{
    [Category("Unit")]
    class a_load_on_start
    {
        [Test]
        public void workshop_items_is_not_null_on_construction()
        {
            var los = new LoadOnStart();
            los.WorkshopItems.Should().NotBeNull();
        }

        [Test]
        public void has_get_and_set_present_for_json()
        {
            var los = new LoadOnStart();
            los.WorkshopItems = new Dictionary<ulong, bool> { { 0, true } };
            los.WorkshopItems.TryGetValue(0, out var value).Should().BeTrue();
        } 
    }
}