﻿namespace SteamQueries.Models
{
    public class SteamItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string[] Tags { get; set; }
        public string PreviewImageUrl { get; set; }
        public ulong NumSubscriptions { get; set; }
        public User Owner { get; set; }
        public ulong PublishFieldId { get; set; }
        public ItemMetaData MetaData { get; set; }
        public string Directory { get; set; }
        public ulong[] DependenciesIds { get; set; }
        public bool IsInstalled { get; set; }
    }
}