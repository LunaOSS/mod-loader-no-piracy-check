﻿namespace SteamQueries.Models
{
    public class ItemMetaData
    {
        public string DllName { get; set; }
        public bool AllowLoadOnStart { get; set; }
        public bool ShowOnMainList { get; set; }
        public string DllHash { get; set; }
    }
}