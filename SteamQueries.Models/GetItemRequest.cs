﻿namespace SteamQueries.Models
{
    public class GetItemRequest : IMessage
    {
        public ulong PublishFieldId { get; set; }
        public string MessageType { get; set; } = nameof(GetItemRequest);
    }
}