﻿using System.Collections.Generic;

namespace SteamQueries.Models
{
    public class GetSubscribedItemsResponse : IMessage
    {
        public List<SteamItem> Items { get; set; }
        public bool HasValues { get; set; }
        public string MessageType { get; set; } = nameof(GetSubscribedItemsResponse);
    }
}