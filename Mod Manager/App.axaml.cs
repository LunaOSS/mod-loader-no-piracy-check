using System;
using System.IO.Abstractions;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.ConfigParser;
using Mod_Manager.Abstractions.Steamworks;
using Mod_Manager.Abstractions.VIewModel;
using Mod_Manager.Models;
using Mod_Manager.Utilities;
using Mod_Manager.ViewModels;
using Mod_Manager.Views;
using Mod_Manager.Wrappers.ConfigParser;
using Mod_Manager.Wrappers.Steamworks;
using MsBox.Avalonia;
using MsBox.Avalonia.Dto;
using MsBox.Avalonia.Enums;
using Serilog;
using Serilog.Formatting.Compact;
using Steamworks;

namespace Mod_Manager;

public partial class App : Application
{
    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    public override async void OnFrameworkInitializationCompleted()
    {
        var builder = Host.CreateApplicationBuilder();
            
        // Models
        builder.Services.AddSingleton<IFileSystem, FileSystem>();
        builder.Services.AddSingleton<ISteamRemoteStorage, Wrappers.Steamworks.SteamRemoteStorage>();
        builder.Services.AddSingleton<IQuery, Query>();
        builder.Services.AddSingleton<ILoadOnStartManager, LoadOnStartManager>();
        builder.Services.AddSingleton<IRuntimeInfo, RuntimeInfo>();
        builder.Services.AddSingleton<IFileManager, FileManager>();
        builder.Services.AddSingleton<IConfigParser, WrappedConfigParser>();
        builder.Services.AddSingleton<IDoorstopManager, DoorstopManager>();
        builder.Services.AddSingleton<IInit, Init>();
        builder.Services.AddSingleton<IHttp, Http>();
        builder.Services.AddSingleton<ILogsZipGenerator, LogsZipGenerator>();
        builder.Services.AddSingleton<IProcess, Process>();

        // View Models
        builder.Services.AddSingleton<IHeaderBarViewModel, HeaderBarViewModel>();
        builder.Services.AddSingleton<IHomeViewModel, HomeViewModel>();
        builder.Services.AddSingleton<IMainWindowViewModel, MainWindowViewModel>();

        using var host = builder.Build();
            
        var services = host.Services;
        var fileSystem = services.GetService<IFileSystem>();
        CreateLogger(fileSystem);
        StartSteam();
            
        var window = new MainWindow { DataContext = services.GetService<IMainWindowViewModel>() };

        if (ApplicationLifetime is not IClassicDesktopStyleApplicationLifetime desktop)
        {
            base.OnFrameworkInitializationCompleted();
            return;
        }
        desktop.Exit += DesktopOnExit;
        await StartUpLogic(services.GetService<IHomeViewModel>(),
            services.GetService<IHeaderBarViewModel>(),
            services.GetService<IQuery>(),
            services.GetService<IInit>(),
            services.GetService<IFileManager>(),
            fileSystem,
            services.GetService<ILoadOnStartManager>(),
            desktop,
            window);
    }

    internal async Task StartUpLogic(IHomeViewModel homeViewModel,
        IHeaderBarViewModel headerBarViewModel,
        IQuery steamQuery,
        IInit init,
        IFileManager fileManager,
        IFileSystem fileSystem,
        ILoadOnStartManager losManager,
        IClassicDesktopStyleApplicationLifetime desktop, 
        MainWindow mainWindow)
    {
        if (!init.IsInGameFiles())
        {
            var messageBoxParams = new MessageBoxStandardParams
            {
                ButtonDefinitions = ButtonEnum.Ok,
                ContentTitle = "Can't find VTOL VR",
                ContentMessage = "Please install the Mod Loader in the same drive as the game VTOL VR"
            };
            await MessageBoxManager.GetMessageBoxStandard(messageBoxParams).ShowAsync();
            desktop.Shutdown(2);
            return;
        }
            
        desktop.MainWindow = mainWindow;
        var loadOnStartSettings = losManager.GetLoadOnStartItems();
        var currentFolder = fileSystem.Directory.GetCurrentDirectory();
        var parentFolder = fileSystem.DirectoryInfo.FromDirectoryName(currentFolder).Parent;
        var currentState = fileManager.GetCurrentState(parentFolder.FullName);
        init.SetCurrentState(currentState);
        headerBarViewModel.UpdateToggleButton();
            
        await homeViewModel.GetWorkshopItems(steamQuery, loadOnStartSettings);
        base.OnFrameworkInitializationCompleted();
    }

    private void DesktopOnExit(object? sender, ControlledApplicationLifetimeExitEventArgs e)
    {
        SteamClient.Shutdown();
    }

    private static void CreateLogger(IFileSystem fileSystem)
    {
        var currentTime = DateTime.Now;
        var logPath = LogsDirectoryName + $"/Log_{currentTime.Ticks}.txt";
            
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Debug()
            .WriteTo.File(new CompactJsonFormatter(), logPath, shared:true)
            .CreateLogger();

        var currentDir = fileSystem.Directory.GetCurrentDirectory();
        Log.Information("Program Started in '{CURRENTFOLDER}'", currentDir);
    }

    private static void StartSteam()
    {
        Log.Information("Starting Steam");
        try
        {
            SteamClient.Init(244850);
        }
        catch (Exception e)
        {
            // Catching error in Rider previewer
        }
    }

    public const string LogsDirectoryName = "Logs";
}