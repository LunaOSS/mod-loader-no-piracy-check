﻿using System.Collections.Generic;

namespace Mod_Manager.Data;

internal sealed class LoadOnStart
{
    public Dictionary<ulong, bool> WorkshopItems { get; set; } = new();
}