﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Mod_Manager.Abstractions.Steamworks;

namespace Mod_Manager.Models;

public class Items
{
    private readonly IQuery _query;
    public Items(IQuery query)
    {
        _query = query;
    }
    
    public async Task<IEnumerable<Item>> GetSubscribedItemsAsync(int page)
    {
        var query = _query.WhereUserSubscribed();
        var resultPage = await query.GetPageAsync(page);
        
        var returnList = new List<Item>(resultPage.ResultCount);
        foreach (var newItem in resultPage.Entries)
        {
            returnList.Add(new Item(newItem));
        }

        return returnList;
    }

}