﻿using System.Threading.Tasks;
using Mod_Manager.Abstractions.Steamworks;
using Serilog;

namespace Mod_Manager.Models;

public class Item
{
    public string Name;
    public string Description { get; set; }
    public ulong Id { get; set; }
    public IItem SteamItem;
    public string Url;
    public string Author;
    public ulong NumSubscriptions;
    public string OwnerName;
    public string PreviewImageUrl;

    public Item(IItem steamItem)
    {
        SteamItem = steamItem;
        Id = steamItem.Id.Value;
        Name = steamItem.Title;
        Description = steamItem.Description;
        NumSubscriptions = steamItem.NumSubscriptions;
        OwnerName = steamItem.OwnerName;
        Url = $"https://steamcommunity.com/sharedfiles/filedetails/?source=vtolvr-mod-loader&id={Id}";
        PreviewImageUrl = steamItem.PreviewImageUrl;
    }

    public async Task ToggleSubscribedState()
    {
        if (!SteamItem.IsSubscribed)
        {
            Log.Information("Subscribing to {Name}", Name);
            await SteamItem.Subscribe();
            return;
        }
        Log.Information("Unsubscribing to {Name}", Name);
        await SteamItem.Unsubscribe();
    }
}