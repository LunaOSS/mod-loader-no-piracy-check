﻿using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.Steamworks;
using Mod_Manager.Data;
using Newtonsoft.Json;

namespace Mod_Manager.Models;

internal sealed class LoadOnStartManager : ILoadOnStartManager
{
    private readonly ISteamRemoteStorage _steamStorage;
    public const string LoadOnStartFile = "Load on Start";

    public LoadOnStartManager(ISteamRemoteStorage steamStorage)
    {
        _steamStorage = steamStorage;
    }

    public LoadOnStart GetLoadOnStartItems()
    {
        if (!_steamStorage.FileExists(LoadOnStartFile))
        {
            return new LoadOnStart();
        }
        
        var json = _steamStorage.FileReadText(LoadOnStartFile);
        return JsonConvert.DeserializeObject<LoadOnStart>(json);
    }

    public void ChangeStateOnItem(ulong itemId, bool newState)
    {
        var settings = GetLoadOnStartItems();
        settings.WorkshopItems[itemId] = newState;
        var json = JsonConvert.SerializeObject(settings);
        _steamStorage.FileWrite(LoadOnStartFile, json);
    }
}