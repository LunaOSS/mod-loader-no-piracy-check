﻿using System.IO;
using System.IO.Abstractions;
using Mod_Manager.Abstractions;

namespace Mod_Manager.Models;

internal sealed class FileManager : IFileManager
{
    private const string _doorstopConfig = "doorstop_config.ini";
    
    private readonly IFileSystem _fileSystem;

    public FileManager(IFileSystem fileSystem)
    {
        _fileSystem = fileSystem;
    }

    public FilesState GetCurrentState(string vtolPath)
    {
        var doorstopConfigPath = Path.Combine(vtolPath, _doorstopConfig);
        if (!_fileSystem.File.Exists(doorstopConfigPath))
        {
            return FilesState.Vanilla;
        }
        return  FilesState.Modded;
    }

    public string GetVtolDirectory()
    {
        var currentDirectory = _fileSystem.Directory.GetCurrentDirectory();
        var vtolDirectory = _fileSystem.DirectoryInfo.FromDirectoryName(currentDirectory).Parent;
        return vtolDirectory.FullName;
    }
}