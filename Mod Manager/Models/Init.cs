﻿using System;
using System.IO.Abstractions;
using System.Linq;
using System.Threading.Tasks;
using Mod_Manager.Abstractions;

namespace Mod_Manager.Models;

internal sealed class Init : IInit
{
    private readonly IFileSystem _fileSystem;
    private readonly IFileManager _fileManager;
    private readonly IDoorstopManager _doorstopManager;
    private FilesState _currentState = FilesState.Unknown;

    public Init(IFileSystem fileSystem, IFileManager fileManager, IDoorstopManager doorstopManager)
    {
        _fileSystem = fileSystem;
        _fileManager = fileManager;
        _doorstopManager = doorstopManager;
    }

    /// <summary>
    /// Searches the parent folder for a valid VTOL VR directory.
    /// </summary>
    /// <returns>Returns true if it is installed inside VTOL VR's game files</returns>
    public bool IsInGameFiles()
    {
        var modLoaderDirectory =
            _fileSystem.DirectoryInfo.FromDirectoryName(_fileSystem.Directory.GetCurrentDirectory());
        var vtolDirectory = modLoaderDirectory.Parent;

        return IsVtolFolder(vtolDirectory);
    }
    
    public Task EnableModLoader()
    {
        if (_currentState == FilesState.Unknown)
        {
            throw new Exception("Verify Files before enabling the Mod Loader");
        }

        _doorstopManager.Enable();
        _currentState = FilesState.Modded;
        return Task.CompletedTask;
    }

    public void DisableModLoader()
    {
        if (_currentState == FilesState.Unknown)
        {
            throw new Exception("Verify Files before disabling the Mod Loader");
        }
        _doorstopManager.Disable();
        _currentState = FilesState.Vanilla;
    }

    public void VerifyFiles()
    {
        
    }
    
    public bool IsVtolFolder(IDirectoryInfo folder)
    {
        // Checks for 'VTOLVR.exe' and 'VTOLVR_Data' in the root
        
        const string vtolExeName = "VTOLVR.exe";
        if (!folder.GetFiles().Any(f => f.Name.Equals(vtolExeName)))
        {
            return false;
        }

        const string dataFolderName = "VTOLVR_Data";
        var subFolders = folder.GetDirectories();
        return subFolders.Any(directory => directory.Name.Equals(dataFolderName));
    }

    public FilesState GetCurrentState() => _currentState;

    public void SetCurrentState(FilesState newState) => _currentState = newState;
}