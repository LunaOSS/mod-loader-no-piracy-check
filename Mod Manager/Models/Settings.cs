﻿using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.Steamworks;
using Newtonsoft.Json;

namespace Mod_Manager.Models;

internal sealed class Settings: ISettings
{
    private const string fileName = "settings.json";
    private ISteamRemoteStorage _steamStorage;
    
    public void SetLastVTOLPath(string newPath)
    {
        var settings = LoadSettings();
        settings.VtolPath = newPath;
        SaveSettings(settings);
    }

    public string GetLastVTOLPath()
    {
        var settings = LoadSettings();
        return settings.VtolPath;
    }

    public Settings(ISteamRemoteStorage steamStorage)
    {
        _steamStorage = steamStorage;
    }

    private SettingsJson LoadSettings()
    {
        if (!_steamStorage.FileExists(fileName))
        {
            return new SettingsJson();
        }

        var fileText = _steamStorage.FileReadText(fileName);
        var convertedObject = JsonConvert.DeserializeObject<SettingsJson>(fileText);
        return convertedObject;
    }

    private void SaveSettings(SettingsJson settingsClass)
    {
        var jsonText = JsonConvert.SerializeObject(settingsClass, Formatting.Indented);
        _steamStorage.FileWrite(fileName, jsonText);
    }

    private class SettingsJson
    {
        [JsonProperty("vtol_path")]
        public string VtolPath;
    }
}