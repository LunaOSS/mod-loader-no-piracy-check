﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.Steamworks;
using Serilog;

namespace Mod_Manager.Models;

internal sealed class LogsZipGenerator: ILogsZipGenerator
{
    private readonly IRuntimeInfo _runtimeInfo;
    private readonly IFileSystem _fileSystem;
    private readonly ISteamRemoteStorage _steamRemoteStorage;

    public LogsZipGenerator(IRuntimeInfo runtimeInfo, IFileSystem fileSystem, ISteamRemoteStorage steamRemoteStorage)
    {
        _runtimeInfo = runtimeInfo;
        _fileSystem = fileSystem;
        _steamRemoteStorage = steamRemoteStorage;
    }

    public async Task<string> CollectLogs()
    {
        Log.Information("Collecting Logs");
        var fileFriendlyDateTime = DateTime.Now.ToString().Replace('/', '-').Replace(':', '-');
        var zipFilePath = Path.Combine(_fileSystem.Directory.GetCurrentDirectory(), $"Logs {fileFriendlyDateTime}.zip");
        using var archive = ZipFile.Open(zipFilePath, ZipArchiveMode.Create);

        var playerLogPath = GetPlayerLogPath();
        if (!string.IsNullOrEmpty(playerLogPath))
        {
            try
            {
                archive.CreateEntryFromFile(GetPlayerLogPath(), "Player.log");
            }
            catch (Exception e)
            {
                Log.Warning(e, "Unable to copy game log into zip");
            }   
        }

        var doorstopFile = GetDoorstopFile();
        if (!string.IsNullOrEmpty(doorstopFile))
        {
            archive.CreateEntryFromFile(doorstopFile, "doorstop_config.ini");
        }

        var loadOnStartBytes = GetLoadOnStartBytes().ToArray();
        if (loadOnStartBytes.Any())
        {
            var entry = archive.CreateEntry("Load On Start.json");
            await using var stream = entry.Open();
            await stream.WriteAsync(loadOnStartBytes);
        }

        var infoEntry = archive.CreateEntry("Info.txt");
        await using (var infoStream = infoEntry.Open())
        {
            await infoStream.WriteAsync(Encoding.Default.GetBytes(PrintGenericInfo()));
        }
        
        // This should be last so that any log messages in the steps above get shown
        var pastThreeDaysLogs = GetLastXModLoaderLogs(TimeSpan.FromDays(3));
        foreach (var logInfo in pastThreeDaysLogs)
        {
            var entry = archive.CreateEntry($"Mod Manager Logs/{logInfo.Name}");
            entry.LastWriteTime = logInfo.LastWriteTime;
            
            await using var fileStream = _fileSystem.File.Open(logInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            await using var stream = entry.Open();
            await fileStream.CopyToAsync(stream);
        }
        
        return zipFilePath;
    }

    private string GetPlayerLogPath()
    {
        if (_runtimeInfo.IsOSPlatform(OSPlatform.Linux))
        {
            var home = _fileSystem.DirectoryInfo.New(
                Environment.GetFolderPath(Environment.SpecialFolder.Personal));

            var folder = Path.Combine(home.FullName, ".config", "unity3d", "Boundless Dynamics, LLC", "VTOLVR");
            if (_fileSystem.Directory.Exists(folder))
            {
                var filePath = Path.Combine(folder, "Player.log");
                if (_fileSystem.File.Exists(filePath))
                {
                    return filePath;
                }
                Log.Information("Couldn't find the player.log file at {0}", filePath);
            }
        }
        else if (_runtimeInfo.IsOSPlatform(OSPlatform.Windows))
        {
            var userprofile = _fileSystem.DirectoryInfo.New(
                Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));

            var folder =Path.Combine(userprofile.FullName,"AppData", "LocalLow", "Boundless Dynamics, LLC", "VTOLVR");
            if (_fileSystem.Directory.Exists(folder))
            {
                var filePath = Path.Combine(folder, "Player.log");
                if (_fileSystem.File.Exists(filePath))
                {
                    return filePath;
                }
                Log.Information("Couldn't find the player.log file at {0}", filePath);
            }
        }
        return string.Empty;
    }

    private IEnumerable<IFileSystemInfo> GetLastXModLoaderLogs(TimeSpan timeBack)
    {
        var path = Path.Combine(_fileSystem.Directory.GetCurrentDirectory(), App.LogsDirectoryName);

        if (!_fileSystem.Directory.Exists(path))
        {
            Log.Warning("Could not find log path of {LogPath}", path);
            return Enumerable.Empty<IFileSystemInfo>();
        }

        var files = _fileSystem.DirectoryInfo.New(path).GetFileSystemInfos("*.txt");
        var now = DateTime.Now;
        var recentLogFiles = new List<IFileSystemInfo>();
        
        foreach (var logFile in files)
        {
            if (now.Subtract(logFile.CreationTime) <= timeBack)
            {
                recentLogFiles.Add(logFile);
            }
        }

        return recentLogFiles;
    }

    private string GetDoorstopFile()
    {
        var currentDir = _fileSystem.DirectoryInfo.New(_fileSystem.Directory.GetCurrentDirectory());
        var vtolDir = currentDir.Parent;
        var doorstopPath = Path.Combine(vtolDir.FullName, "doorstop_config.ini");

        if (!_fileSystem.File.Exists(doorstopPath))
        {
            Log.Warning("No doorstop_config.ini was found");
            return string.Empty;
        }

        return doorstopPath;
    }

    private IEnumerable<byte> GetLoadOnStartBytes()
    {
        if (!_steamRemoteStorage.FileExists(LoadOnStartManager.LoadOnStartFile))
        {
            return Enumerable.Empty<byte>();
        }

        return _steamRemoteStorage.FileRead(LoadOnStartManager.LoadOnStartFile);
    }

    private string PrintGenericInfo()
    {
        var assembly = System.Reflection.Assembly.GetExecutingAssembly();
        var fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
        string version = fvi.FileVersion;
        
        var builder = new StringBuilder();

        builder.AppendLine($"Created Time:{DateTime.Now}");
        builder.AppendLine($"UTC Time:{DateTime.UtcNow}");
        builder.AppendLine($"Mod Manager Version:{version}");
        builder.AppendLine($"Current Directory:{_fileSystem.Directory.GetCurrentDirectory()}");

        return builder.ToString();
    }
}