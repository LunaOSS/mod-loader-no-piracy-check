﻿using System;
using System.Diagnostics;
using System.Reactive;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.VIewModel;
using Mod_Manager.Utilities;
using ReactiveUI;

namespace Mod_Manager.ViewModels;
sealed class HeaderBarViewModel : ViewModelBase, IHeaderBarViewModel
{
    private readonly IInit _instance;
    private readonly ILogsZipGenerator _logsZipGenerator;
    private readonly IRuntimeInfo _runtimeInfo;
    private readonly IProcess _process;
    private readonly IHttp _http;
    public ReactiveCommand<Unit, Unit> HelpCommand { get; }
    public ReactiveCommand<Unit, Unit> ToggleCommand { get; }
    public ReactiveCommand<Unit, Unit> CollectLogsCommand { get; }
    
    private string _toggleButtonText = "Loading";
    public string ToggleButtonText
    {
        get => _toggleButtonText;
        set => this.RaiseAndSetIfChanged(ref _toggleButtonText, value); 
    }
    
    private bool _toggleIsDangerColour;
    public bool ToggleIsDangerColour
    {
        get => _toggleIsDangerColour;
        set => this.RaiseAndSetIfChanged(ref _toggleIsDangerColour, value); 
    }
        
    private bool _toggleIsWarningColour;
    public bool ToggleIsWarningColour
    {
        get => _toggleIsWarningColour;
        set => this.RaiseAndSetIfChanged(ref _toggleIsWarningColour, value); 
    }
        
    private bool _toggleIsSuccessColour;
    public bool ToggleIsSuccessColour
    {
        get => _toggleIsSuccessColour;
        set => this.RaiseAndSetIfChanged(ref _toggleIsSuccessColour, value); 
    }
    
    private string _toggleToolTipText;
    public string ToggleToolTipText
    {
        get => _toggleToolTipText;
        set => this.RaiseAndSetIfChanged(ref _toggleToolTipText, value); 
    }

    public HeaderBarViewModel()
    {
        HelpCommand = ReactiveCommand.Create(HelpButtonPressed);
        ToggleCommand = ReactiveCommand.Create(ToggleButtonPressed);
        CollectLogsCommand = ReactiveCommand.CreateFromTask(CreateLogsPressed);
    }

    public HeaderBarViewModel(IInit instance, ILogsZipGenerator logsZipGenerator, IRuntimeInfo runtimeInfo, IProcess process, IHttp http) : this()
    {
        _instance = instance;
        _logsZipGenerator = logsZipGenerator;
        _runtimeInfo = runtimeInfo;
        _process = process;
        _http = http;
    }

    private void ToggleButtonPressed()
    {
        switch (_instance.GetCurrentState())
        {
            case FilesState.Unknown:
                Task.Run(delegate { _instance.VerifyFiles(); });
                break;
            case FilesState.Modded:
                Task.Run(delegate
                {
                    _instance.DisableModLoader();
                    UpdateToggleButton();
                });
                break;
            case FilesState.Vanilla:
                Task.Run(delegate
                {
                    _instance.EnableModLoader();
                    UpdateToggleButton();
                });
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    public void UpdateToggleButton()
    {
        ToggleIsDangerColour = false;
        ToggleIsWarningColour = false;
        ToggleIsSuccessColour = false;
            
        switch (_instance.GetCurrentState())
        {
            case FilesState.Unknown:
                ToggleButtonText = "Unknown";
                ToggleIsWarningColour = true;
                break;
            case FilesState.Modded:
                ToggleButtonText = "Disable";
                ToggleToolTipText = "Press to disable the Mod Loader";
                ToggleIsDangerColour = true;
                break;
            case FilesState.Vanilla:
                ToggleButtonText = "Enable";
                ToggleToolTipText = "Press to enable the Mod Loader";
                ToggleIsSuccessColour = true;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void HelpButtonPressed()
    {
        _http.OpenUrl("https://docs.vtolvr-mods.com/");
    }
    
    private async Task CreateLogsPressed()
    {
        var zipPath = await _logsZipGenerator.CollectLogs();
        if (_runtimeInfo.IsOSPlatform(OSPlatform.Windows))
        {
            _process.Start("explorer.exe", string.Format("/select,\"{0}\"", zipPath));
        }
    }

    public ViewModelBase GetViewModel() => this;
}