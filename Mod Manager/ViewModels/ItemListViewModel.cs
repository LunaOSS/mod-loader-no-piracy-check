﻿using System.Reactive;
using System.Threading.Tasks;
using Avalonia.Media.Imaging;
using Material.Icons;
using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.VIewModel;
using Mod_Manager.Utilities;
using ReactiveUI;

namespace Mod_Manager.ViewModels;

internal sealed class ItemListViewModel : ViewModelBase, IViewModel
{
    public ReactiveCommand<Unit, Unit> OpenInSteam { get; }
    public ReactiveCommand<Unit, Unit> ToggleLoadOnStart { get; }
    public string Name { get; set; } = "[Workshop Item Name]";
    public ulong DownloadCount { get; set; } = 1234;
    public string Author { get; set; } = "[UserName]";
    private Bitmap? _image;
    public Bitmap? Image
    {
        get => _image;
        set => this.RaiseAndSetIfChanged(ref _image, value);
    }
    
    private string _losToolTip;
    public string LosToolTip
    {
        get => _losToolTip;
        set => this.RaiseAndSetIfChanged(ref _losToolTip, value); 
    }
    
    private MaterialIconKind _losIcon;
    public MaterialIconKind LosIcon
    {
        get => _losIcon;
        set => this.RaiseAndSetIfChanged(ref _losIcon, value); 
    }

    private readonly IHttp _http;
    private readonly ILoadOnStartManager _loadOnStartManager;
    private readonly string _previewImageUrl;
    private readonly string _workshopUrl;
    private readonly ulong _itemId;

    public ItemListViewModel()
    {
        OpenInSteam = ReactiveCommand.Create(OpenInSteamPressed);
        ToggleLoadOnStart = ReactiveCommand.Create(ToggleLoadOnStartPressed);
        LosIcon = MaterialIconKind.Rocket;
        LosToolTip = "Enable Load on Start";
    }

    public ItemListViewModel(IHttp http,
        ILoadOnStartManager loadOnStartManager,
        ref string name,
        ulong downloadCount,
        string author,
        string previewImageUrl,
        string workshopUrl,
        bool losEnabled,
        ulong itemId) : this()
    {
        _previewImageUrl = previewImageUrl;
        _workshopUrl = workshopUrl;
        _itemId = itemId;
        Name = name;
        DownloadCount = downloadCount;
        Author = author;
        _http = http;
        _loadOnStartManager = loadOnStartManager;
        SetLoadOnStartUi(losEnabled);
    }
    
    private void OpenInSteamPressed() => _http.OpenInSteam(_workshopUrl);

    private void ToggleLoadOnStartPressed()
    {
        var losEnabled = LosIcon == MaterialIconKind.RocketLaunch;
        _loadOnStartManager.ChangeStateOnItem(_itemId, !losEnabled);
        SetLoadOnStartUi(!losEnabled);
    }

    private void SetLoadOnStartUi(bool state)
    {
        if (state)
        {
            LosToolTip = "Disable Load on Start";
            LosIcon = MaterialIconKind.RocketLaunch;
            return;
        }
        LosToolTip = "Enable Load on Start";
        LosIcon = MaterialIconKind.Rocket;
    }

    public async Task LoadImage()
    {
        Image = await _http.GetImageAsync(_previewImageUrl);
    }

    public ViewModelBase GetViewModel() => this;
}