﻿using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.VIewModel;

namespace Mod_Manager.ViewModels;

internal sealed class MainWindowViewModel : ViewModelBase, IMainWindowViewModel
{
    private readonly IViewModel _headerView;

    public ViewModelBase HeaderBarView
    {
        get => _headerView.GetViewModel();
    }

    private IViewModel _mainView;

    public ViewModelBase MainView
    {
        get => _mainView.GetViewModel();
    }

    public MainWindowViewModel(IHeaderBarViewModel header, IHomeViewModel mainView)
    {
        _headerView = header;
        _mainView = mainView;
    }

    public ViewModelBase GetViewModel() => this;
}
