﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Avalonia.Media.Imaging;
using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.Steamworks;
using Mod_Manager.Abstractions.VIewModel;
using Mod_Manager.Data;
using Mod_Manager.Models;
using ReactiveUI;
using Serilog;
using Item = Mod_Manager.Models.Item;

namespace Mod_Manager.ViewModels;

internal sealed class HomeViewModel : ViewModelBase, IHomeViewModel
{
    private ObservableCollection<ItemListViewModel> _myItems = new ();
    public ObservableCollection<ItemListViewModel> MyItems
    {
        get => _myItems;
        set => this.RaiseAndSetIfChanged(ref _myItems, value); 
    }

    private readonly IHttp _http;
    private readonly ILoadOnStartManager _loadOnStartManager;
    public HomeViewModel(IHttp http, ILoadOnStartManager loadOnStartManager) : this()
    {
        _http = http;
        _loadOnStartManager = loadOnStartManager;
    }
    
    public HomeViewModel()
    {
        
    }

    public async Task GetWorkshopItems(IQuery steamQuery, LoadOnStart loadOnStartSettings)
    {
        var items = new Items(steamQuery);
        var pageItems = await items.GetSubscribedItemsAsync(1);
        var list = pageItems.ToList();
        
        foreach (var item in list)
        {
            var losEnabled = false;
            if (loadOnStartSettings.WorkshopItems.TryGetValue(item.Id, out bool workshopItem))
            {
                losEnabled = workshopItem;
            }
            
            MyItems.Add(new ItemListViewModel(_http,
                _loadOnStartManager,
                ref item.Name,
                item.NumSubscriptions,
                item.OwnerName,
                item.PreviewImageUrl,
                item.Url, 
                losEnabled,
                item.Id));
        }
        
        Log.Information("Got {PageItemCount} subscribed items. My Items is now {MyItemsCount}", list.Count, MyItems.Count);

        await DownloadImages();
    }

    private async Task DownloadImages()
    {
        foreach (var item in MyItems)
        {
            await item.LoadImage();
        }
    }
    
    private void DownloadItemImage(Item item)
    {
        Log.Information("Downloading Preview Image for {Name}", item.Name);
    }

    private void OnDownloaded(Bitmap image)
    {
        MyItems = MyItems;
        Log.Information("Done downloading image");
    }

    public ViewModelBase GetViewModel() => this;
}