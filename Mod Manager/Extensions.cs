﻿using System.IO;
using System.IO.Abstractions;

namespace Mod_Manager;

internal static class Extensions
{
    public static void CopyAllContents(this IDirectoryInfo source, string destination)
    {
        if (!source.FileSystem.Directory.Exists(destination))
        {
            throw new DirectoryNotFoundException(destination);
        }
        
        var outputDir = source.FileSystem.DirectoryInfo.FromDirectoryName(destination);

        var files = source.GetFiles();

        foreach (var file in files)
        {
            var newPath = Path.Combine(outputDir.FullName, file.Name);
            file.CopyTo(newPath);
        }

        var subDirs = source.GetDirectories();

        foreach (var dir in subDirs)
        {
            var newDir = outputDir.CreateSubdirectory(dir.Name);
            dir.CopyAllContents(newDir.FullName);
        }
    }
}