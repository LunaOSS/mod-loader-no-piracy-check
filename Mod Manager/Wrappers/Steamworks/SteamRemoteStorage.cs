﻿using System.Text;
using Mod_Manager.Abstractions.Steamworks;

namespace Mod_Manager.Wrappers.Steamworks;

internal sealed class SteamRemoteStorage : ISteamRemoteStorage
{
    public byte[] FileRead(string fileName)
    {
        return global::Steamworks.SteamRemoteStorage.FileRead(fileName);
    }

    public string FileReadText(string fileName)
    {
        var bytes = FileRead(fileName);
        return Encoding.ASCII.GetString(bytes);
    }

    public bool FileWrite(string fileName, byte[] data)
    {
        return global::Steamworks.SteamRemoteStorage.FileWrite(fileName, data);
    }

    public bool FileWrite(string fileName, string fileText)
    {
        var bytes = Encoding.ASCII.GetBytes(fileText);
        return FileWrite(fileName, bytes);
    }

    public bool FileExists(string fileName)
    {
        return global::Steamworks.SteamRemoteStorage.FileExists(fileName);
    }
}