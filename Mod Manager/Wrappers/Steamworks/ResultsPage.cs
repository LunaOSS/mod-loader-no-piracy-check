﻿using System.Collections.Generic;
using Mod_Manager.Abstractions.Steamworks;

namespace Mod_Manager.Wrappers.Steamworks;

public class ResultsPage : IResultPage
{
    public int ResultCount { get; }
    public IEnumerable<IItem> Entries { get; }

    public ResultsPage(int resultCount, IEnumerable<IItem> entries)
    {
        ResultCount = resultCount;
        Entries = entries;
    }
}