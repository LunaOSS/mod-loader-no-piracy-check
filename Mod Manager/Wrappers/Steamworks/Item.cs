﻿using System;
using System.Threading.Tasks;
using Mod_Manager.Abstractions.Steamworks;

namespace Mod_Manager.Wrappers.Steamworks;

public class Item : IItem
{
    public IPublishedFieldId Id { get; }
    public string Title { get; }
    public string Description { get; }
    public bool IsSubscribed { get; }
    public string[] Tags { get; }
    public string PreviewImageUrl { get; }
    public uint VotesDown { get; }
    public uint VotesUp { get; }
    public ulong NumSubscriptions { get; }
    public string OwnerName { get; }
    public DateTime Updated { get; }

    private global::Steamworks.Ugc.Item _item;

    public Item(global::Steamworks.Ugc.Item item)
    {
        Id = new PublishedFieldId(item.Id);
        Title = item.Title;
        Description = item.Description;
        IsSubscribed = item.IsSubscribed;
        Tags = item.Tags;
        PreviewImageUrl = item.PreviewImageUrl;
        VotesDown = item.VotesDown;
        VotesUp = item.VotesUp;
        Updated = item.Updated;
        NumSubscriptions = item.NumSubscriptions;
        OwnerName = item.Owner.Name;
        _item = item;
    }

    public async Task<bool> Subscribe()
    {
        return await _item.Subscribe();
    }

    public async Task<bool> Unsubscribe()
    {
        return await _item.Unsubscribe();
    }
}