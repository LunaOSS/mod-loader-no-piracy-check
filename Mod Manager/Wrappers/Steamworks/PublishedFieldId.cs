﻿using Mod_Manager.Abstractions.Steamworks;
using Steamworks.Data;

namespace Mod_Manager.Wrappers.Steamworks;

public class PublishedFieldId : IPublishedFieldId
{
    public ulong Value { get; }

    public PublishedFieldId(PublishedFileId publishedFileId)
    {
        Value = publishedFileId.Value;
    }
}