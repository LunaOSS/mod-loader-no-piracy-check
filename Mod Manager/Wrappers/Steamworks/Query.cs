﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Mod_Manager.Abstractions.Steamworks;
using Steamworks;

namespace Mod_Manager.Wrappers.Steamworks;

public class Query : IQuery
{
    private global::Steamworks.Ugc.Query _query;

    public Query()
    {
        _query = global::Steamworks.Ugc.Query.Items;
    }
    public IQuery WhereUserSubscribed()
    {
        _query = _query.WhereUserSubscribed();
        return this;
    }

    public IQuery WithLongDescription(bool b)
    {
        _query = _query.WithLongDescription(b);
        return this;
    }

    public async Task<IResultPage?> GetPageAsync(int page)
    {
        var result = await _query.GetPageAsync(page);
        
        if (!result.HasValue)
        {
            return null;
        }

        List<Item> entries = new (result.Value.ResultCount);
        foreach (var item in result.Value.Entries)
        {
            SteamFriends.RequestUserInformation(item.Owner.Id);
            entries.Add(new Item(item));
        }

        return new ResultsPage(result.Value.ResultCount, entries);
    }
    
    public IQuery WithFileId(ulong id)
    {
        _query = _query.WithFileId(id);
        return this;
    }
}