﻿namespace Mod_Manager.Abstractions;

internal interface ISettings
{
    public void SetLastVTOLPath(string newPath);
    public string GetLastVTOLPath();
}