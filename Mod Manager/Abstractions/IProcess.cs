﻿using System.Diagnostics;

namespace Mod_Manager.Abstractions;

public interface IProcess
{
    void Start(string fileName);
    void Start(string fileName, string arguments);
    void Start(ProcessStartInfo startInfo);
}