﻿namespace Mod_Manager.Abstractions.ConfigParser;

internal interface IConfigParser
{
    void SetFile(string filePath);
    bool GetValue(string sectionName, string keyName, bool defaultValue);
    bool SectionExists(string sectionName);
    bool SetValue(string sectionName, string keyName, bool value);
    bool Save();
}