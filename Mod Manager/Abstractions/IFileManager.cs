﻿using System;

namespace Mod_Manager.Abstractions;

internal interface IFileManager
{
    FilesState GetCurrentState(string vtolPath);
    string GetVtolDirectory();
}