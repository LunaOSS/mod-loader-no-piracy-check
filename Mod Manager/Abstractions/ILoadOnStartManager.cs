﻿using Mod_Manager.Data;

namespace Mod_Manager.Abstractions;

internal interface ILoadOnStartManager
{
    LoadOnStart GetLoadOnStartItems();
    void ChangeStateOnItem(ulong itemId, bool newState);
}