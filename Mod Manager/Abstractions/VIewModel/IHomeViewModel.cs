﻿using System.Threading.Tasks;
using Mod_Manager.Abstractions.Steamworks;
using Mod_Manager.Data;

namespace Mod_Manager.Abstractions.VIewModel;

internal interface IHomeViewModel : IViewModel
{
    Task GetWorkshopItems(IQuery steamQuery, LoadOnStart loadOnStartSettings);
}