﻿namespace Mod_Manager.Abstractions;

public interface IDoorstopManager
{
    void Enable();
    void Disable();
    bool IsEnabled();
}