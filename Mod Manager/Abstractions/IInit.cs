﻿using System.IO.Abstractions;
using System.Threading.Tasks;

namespace Mod_Manager.Abstractions;

internal interface IInit
{
    bool IsInGameFiles();
    Task EnableModLoader();
    void DisableModLoader();
    void VerifyFiles();
    bool IsVtolFolder(IDirectoryInfo folder);
    FilesState GetCurrentState();
    void SetCurrentState(FilesState newState);
}