﻿namespace Mod_Manager.Abstractions.Steamworks;

public interface IPublishedFieldId
{
    public ulong Value { get; }
}