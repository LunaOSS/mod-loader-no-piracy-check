﻿using System.Collections.Generic;

namespace Mod_Manager.Abstractions.Steamworks;

public interface IResultPage
{
    public int ResultCount { get; }
    public IEnumerable<IItem> Entries { get; }
}