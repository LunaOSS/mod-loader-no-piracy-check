﻿using System.Threading.Tasks;

namespace Mod_Manager.Abstractions.Steamworks;

public interface IQuery
{
    public IQuery WhereUserSubscribed();
    
    public IQuery WithLongDescription(bool b);

    public Task<IResultPage?> GetPageAsync(int page);
    
    public IQuery WithFileId(ulong id);
}