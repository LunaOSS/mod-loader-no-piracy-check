﻿namespace Mod_Manager.Abstractions.Steamworks;

internal interface ISteamRemoteStorage
{
    byte[] FileRead(string fileName);
    string FileReadText(string fileName);
    bool FileWrite(string fileName, byte[] data);
    bool FileWrite(string fileName, string fileText);
    bool FileExists(string fileName);
}