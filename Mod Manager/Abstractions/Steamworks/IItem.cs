﻿using System;
using System.Threading.Tasks;

namespace Mod_Manager.Abstractions.Steamworks;

public interface IItem
{
    public IPublishedFieldId Id { get; }
    public string Title { get; }
    public string Description { get; }
    public bool IsSubscribed { get; }
    public string[] Tags { get; }
    public string PreviewImageUrl { get; }
    public uint VotesDown { get; }
    public uint VotesUp { get; }
    public ulong NumSubscriptions { get; }
    public string OwnerName { get; }
    public DateTime Updated { get; }

    public Task<bool> Subscribe();

    public Task<bool> Unsubscribe();
}