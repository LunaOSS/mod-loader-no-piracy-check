﻿using System.Diagnostics;
using Mod_Manager.Abstractions;

namespace Mod_Manager.Utilities;

public class Process : IProcess
{
    public void Start(string fileName) => System.Diagnostics.Process.Start(fileName);
    public void Start(string fileName, string arguments) => System.Diagnostics.Process.Start(fileName, arguments);
    public void Start(ProcessStartInfo startInfo) => System.Diagnostics.Process.Start(startInfo);
}