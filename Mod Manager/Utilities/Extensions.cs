﻿using System.Text;

namespace Mod_Manager.Utilities;

public static class Extensions
{
    public static string ToText(this string[] array)
    {
        if (array == null)
        {
            return "[]";
        }
        
        var builder = new StringBuilder("[");
        for (int i = 0; i < array.Length; i++)
        {
            builder.Append(array[i]);
            if (i != array.Length - 1)
            {
                builder.Append(',');
            }
        }

        builder.Append(']');

        return builder.ToString();
    }
}