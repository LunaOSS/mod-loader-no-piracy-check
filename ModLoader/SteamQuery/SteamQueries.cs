﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Net.NetworkInformation;
using SimpleTCP;
using SteamQueries.Models;
using UnityEngine;
using Valve.Newtonsoft.Json;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace ModLoader.SteamQuery
{
    internal class SteamQueries : MonoBehaviour
    {
        private SimpleTcpClient _tcpClient;
        private const int _startingPort = 1234;
        
        private IEnumerator Start()
        {
            DontDestroyOnLoad(this);
            
            var port = FindFreePort();
            var process = new ProcessStartInfo
            {
                Arguments = $"-a 1067790 -p {port}",
                CreateNoWindow = false,
                FileName = "@Mod Loader\\SteamQueries\\SteamQueries.exe",
                WorkingDirectory = "@Mod Loader\\SteamQueries"
            };
            Process.Start(process);
            Debug.Log($"Started Steam Queries on port {port}");

            yield return new WaitForSeconds(5);
            _tcpClient = new SimpleTcpClient();
            try
            {
                _tcpClient.Connect("127.0.0.1", port);
                _tcpClient.DataReceived += (sender, message) => { Debug.Log(message.MessageString); };
            }
            catch (Exception e)
            {
                Debug.LogError($"Failed to connect to TCP Server {e.Message}");
            }
        }

        private int FindFreePort(int port = _startingPort)
        {
            var isAvailable = true;
            var ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            var tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

            foreach (var tcpi in tcpConnInfoArray)
            {
                if (tcpi.LocalEndPoint.Port==port)
                {
                    isAvailable = false;
                    break;
                }
            }

            if (!isAvailable)
            {
                Debug.Log($"Failed to find free port on: {port}.");
                return FindFreePort(Random.Range(1000, 9999));
            }

            return port;
        }
        
        private void OnDestroy()
        {
            _tcpClient.Disconnect();
        }

        public GetSubscribedItemsResponse GetSubscribedItems(int page)
        {
            var reposeReceived = false;
            
            Debug.Log($"Requesting user's subscribed items on page {page}");
            var message = new GetSubscribedItemsRequest { Page = page };
            var messageText = JsonConvert.SerializeObject(message, Formatting.None);
            var reply = _tcpClient.WriteLineAndGetReply(messageText, TimeSpan.FromSeconds(3));

            if (reply == null)
            {
                Debug.LogError("There was no response");
                return null;
            }
            
            var response = default(IMessage);
            try
            {
                response = JsonConvert.DeserializeObject<IMessage>(reply.MessageString);
            }
            catch (Exception e)
            {
                Debug.LogError("Response was not JSON. " + e);
                return null;
            }
            
            if (response is not GetSubscribedItemsResponse getSubscribedItemsResponse)
            {
                Debug.LogError($"Message back was not of type {nameof(GetSubscribedItemsResponse)}");
                return null;
            }
            
            if (!getSubscribedItemsResponse.HasValues)
            {
                Debug.LogError("Message back had no values");
                return null;
            }

            return getSubscribedItemsResponse;
        }

        public GetItemResponse GetItem(ulong publishedFieldId)
        {
            var reposeReceived = false;
            
            Debug.Log($"Requesting item details {publishedFieldId}");
            var message = new GetItemRequest { PublishFieldId = publishedFieldId };
            var messageText = JsonConvert.SerializeObject(message, Formatting.None);
            var reply = _tcpClient.WriteLineAndGetReply(messageText, TimeSpan.FromSeconds(3));

            if (reply == null)
            {
                Debug.LogError("There was no response");
                return null;
            }
            
            var response = default(IMessage);
            try
            {
                response = JsonConvert.DeserializeObject<IMessage>(reply.MessageString);
            }
            catch (Exception e)
            {
                Debug.LogError("Response was not JSON. " + e);
                return null;
            }
            
            if (response is not GetItemResponse getItemResponse)
            {
                Debug.LogError($"Message back was not of type {nameof(GetItemResponse)}");
                return null;
            }
            
            if (!getItemResponse.HasValues)
            {
                Debug.LogError("Message back had no values");
                return null;
            }

            return getItemResponse;
        }
    }
}