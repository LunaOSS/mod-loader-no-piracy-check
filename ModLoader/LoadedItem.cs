﻿using System.Collections.Generic;
using SteamQueries.Models;

namespace ModLoader
{
    internal record LoadedItem
    {
        public SteamItem Item { get; set; }
        public List<ulong> ItemsThatDependOnThis { get; set; }
    }
}