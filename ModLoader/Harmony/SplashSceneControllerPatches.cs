﻿using HarmonyLib;
using Serilog;
using UnityEngine;
using UnityEngine.CrashReportHandler;

namespace ModLoader.Harmony
{
    [HarmonyPatch(typeof(SplashSceneController), nameof(SplashSceneController.Start))]
    public class SplashSceneController_Start
    {
        public static void Prefix()
        {
            CrashReportHandler.enableCaptureExceptions = false;
            if (GameStartup.version.releaseType != GameVersion.ReleaseTypes.Modded)
            {
                GameStartup._version.releaseType = GameVersion.ReleaseTypes.Modded;
            }
            Log.Information("GameVersion: {GameVersion}", GameStartup.version);
            GameStartup._versionSet = false;
            Debug.Log("Changed to version:" + GameStartup.versionString);
            new GameObject(nameof(TypeInstantiator), typeof(TypeInstantiator));
        }
    }
}