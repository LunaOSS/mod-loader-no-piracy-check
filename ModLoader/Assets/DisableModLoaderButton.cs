﻿using System;
using UnityEngine;

namespace ModLoader.Assets
{
    internal class DisableModLoaderButton : MonoBehaviour
    {
        private void Start()
        {
            var gameObject = AssetBundleLoader.Instance.SpawnPrefab("Disable Mod Loader Button");
            var gamePanel = GameObject.Find("GamePanel");
            if (gamePanel == null)
            {
                // Platoons/CarrierPlatoon/AlliedCarrier/ControlPanel (1)/EquipPanel/Canvas/main/GamePanel
                throw new Exception("GamePanel could not be found to spawn Mod Loader's disable button.");
            }

            gameObject.transform.parent = gamePanel.transform;
            gameObject.transform.localPosition = new Vector3(41.9500008f, -63.8899994f, -2);
            gameObject.transform.localRotation = Quaternion.Euler(0,-90,0);
            gameObject.transform.localScale = new Vector3(423.419189f, 423.420349f, 423.418701f);
            
            var interactable = gameObject.GetComponent<VRInteractable>();
            interactable.OnInteract.AddListener(Disable);
        }
        
        private void Disable()
        {
            //Validation.DisableDoorstop();
            Debug.Log("Disabled Mod Loader");
            Application.Quit();
        }
    }
}