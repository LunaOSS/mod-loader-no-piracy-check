﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ModLoader.IntegratedMods
{
    internal sealed class MouseVRInteract : MonoBehaviour
    {
        private VRInteractable _hoveredInteractable;
        
        private void Update()
        {
            GetIntractable();
            ClickInteractable();
        }

        private void GetIntractable()
        {
            Camera camera = FindCamera();

            Ray ray = camera.ScreenPointToRay(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);

            List<VRInteractable> intersectedInteractables = new();

            var allInteractables = GameObject.FindObjectsOfType<VRInteractable>(false);
            
            foreach (VRInteractable interactable in allInteractables) 
            {
                if (interactable == null || interactable.transform == null)
                    continue;

                float radius = Mathf.Min(Mathf.Max(0.01f, interactable.radius), 0.1f); 
                var bounds = new Bounds(interactable.transform.position, Vector3.one * radius);

                if (bounds.IntersectRay(ray))
                {
                    intersectedInteractables.Add(interactable);
                }
            }
            
            float depth = 0.5f;
            _hoveredInteractable = intersectedInteractables
                .Where(x => x != null && x.transform != null)
                .OrderBy((x) => Vector3.Distance(x.transform.position, ray.origin + (ray.direction * depth)))
                .FirstOrDefault();
        }

        private Camera FindCamera()
        {
            var head = VRHead.instance.gameObject;
            if (!head.name.Equals("Camera (eye)", StringComparison.OrdinalIgnoreCase))
            {
                LogError($"could not find camera it was expecting on object '{head.name}'");
                return null;
            }

            return head.GetComponent<Camera>();
        }

        private void ClickInteractable()
        {
            if (_hoveredInteractable == null)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                Log($"Clicking on interactable {_hoveredInteractable.interactableName}");
                _hoveredInteractable.StartInteraction();
            }

            if (Input.GetMouseButtonUp(0))
            {
                Log($"Releasing on interactable {_hoveredInteractable.interactableName}");
                _hoveredInteractable.StopInteraction();
            }
        }

        private void OnGUI()
        {
            if (_hoveredInteractable == null)
            {
                return;
            }
            
            GUI.Label(new Rect(10, 10, 100, 20), _hoveredInteractable.interactableName);
        }
        
        private void LogError(object message)
        {
            Debug.LogError($"[{nameof(MouseVRInteract)}]{message}");
        }

        private void Log(object message)
        {
            Debug.Log($"[{nameof(MouseVRInteract)}]{message}");
        }
    }
}