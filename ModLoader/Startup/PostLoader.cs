﻿using System;
using Serilog;

namespace ModLoader.Startup
{
    internal static class PostLoader
    {
        public static void PatchHarmony()
        {
            Log.Information("Patching Harmony");
            var harmony = new HarmonyLib.Harmony("vtolvr.modloader");
            try
            {
                harmony.PatchAll();
            }
            catch (Exception e)
            {
                Log.Error(e, "Failed to Patch All");
            }
        }
    }
}