﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using ModLoader.Framework;
using ModLoader.Framework.Attributes;
using ModLoader.Framework.Exceptions;
using SteamQueries.Models;
using UnityEngine;

namespace ModLoader
{
    internal class ModLoader : MonoBehaviour
    {
        public static ModLoader Instance { get; private set; }

        private readonly Dictionary<ulong, LoadedItem> _loadedItems = new ();
        private SteamQuery.SteamQueries _steamQueries;
        
        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(this);
            _steamQueries = GameObject.FindObjectOfType<SteamQuery.SteamQueries>();
        }

        public bool LoadSteamItem(SteamItem item)
        {
            if (IsItemLoaded(item.PublishFieldId))
            {
                Debug.LogWarning($"'{item.Title}' is already loaded");
                return true;
            }

            return LoadMod(item);
        }

        /// <summary>
        /// Destroys any game objects of the passed in item and if a dependency isn't required by anything else it will also destroy its game objects
        /// </summary>
        /// <param name="item"></param>
        public void DisableSteamItem(SteamItem item)
        {
            if (!IsItemLoaded(item.PublishFieldId))
            {
                Debug.LogWarning($"'{item.Title} isn't loaded");
                return;
            }

            RemoveItemGameObjects(item);
        }

        private void RemoveItemGameObjects(SteamItem item)
        {
            Debug.Log($"Disabling '{item.Title}'");
            var assembly = GetSteamItemAssembly(item);
            if (ContainsVtolMod(assembly, out var types))
            {
                foreach (Type type in types)
                {
                    var typeGameObject = GameObject.FindObjectOfType(type, true);
                    if (typeGameObject == null)
                    {
                        Debug.LogError($"Couldn't find any game objects of type '{type.FullName}'");
                        continue;
                    }
                    VtolMod mod = typeGameObject as VtolMod;
                    if (mod == null)
                    {
                        Debug.LogError($"Couldn't convert '{type.Name}' to {nameof(VtolMod)}");
                        continue;
                    }
                    mod.UnLoad(); // Coroutines won't have finished but oh well
                    Destroy(mod.gameObject);
                }
            }
            
            _loadedItems.Remove(item.PublishFieldId);

            if (item.DependenciesIds == null || !item.DependenciesIds.Any())
            {
                return;
            }

            Debug.Log($"Disabling Dependencies of '{item.Title}'");
                
            foreach (var publishFieldId in item.DependenciesIds)
            {
                var response = _steamQueries.GetItem(publishFieldId);
                    
                // In Facepunch Steamworks, WithFileId returns a list of items.
                // This just assumes there could be more than one, even though we are searching the exact id value
                    
                foreach (var steamItem in response.Items)
                {
                    if (!_loadedItems.TryGetValue(steamItem.PublishFieldId, out var record))
                    {
                        Debug.LogError("Attempted to unload a dependency however could not find it in the load of loaded items");
                        continue;
                    }

                    if (record.ItemsThatDependOnThis.All(dependant => dependant.Equals(item.PublishFieldId)))
                    {
                        RemoveItemGameObjects(steamItem);
                    }
                    else
                    {
                        var idsToString = string.Join(",",
                            record.ItemsThatDependOnThis.Where(d => !d.Equals(item.PublishFieldId)));
                        Debug.Log($"Not disabling '{steamItem.Title}' as these publish field ids [{idsToString}] depend on it");
                    }
                }
            }
        }

        public bool IsItemLoaded(ulong publishedFieldId) => _loadedItems.ContainsKey(publishedFieldId);

        private bool LoadMod(SteamItem item, ulong itemReasonId = 0)
        {
            if (item.DependenciesIds != null && item.DependenciesIds.Any())
            {
                Debug.Log($"Loading Dependencies of '{item.Title}'");
                
                foreach (var publishFieldId in item.DependenciesIds)
                {
                    var response = _steamQueries.GetItem(publishFieldId);
                    
                    // In Facepunch Steamworks, WithFileId returns a list of items.
                    // This just assumes there could be more than one, even though we are searching the exact id value
                    
                    foreach (var steamItem in response.Items)
                    {
                        if (!LoadMod(steamItem, item.PublishFieldId))
                        {
                            // Stops loading the original assembly if the dependencies fail to load
                            return false;
                        }
                    }
                }
            }
            
            Debug.Log($"Loading '{item.Title}'");
            try
            {
                var assembly = LoadAssembly(item);
                
                if (!_loadedItems.TryGetValue(item.PublishFieldId, out var record))
                {
                    if (ContainsVtolMod(assembly, out var types))
                    {
                        foreach (var type in types)
                        {
                            SpawnVtolMod(type, assembly, item);
                        }
                    }

                    var list = new List<ulong>();
                    if (itemReasonId != 0)
                    {
                        list.Add(itemReasonId);
                    }
                    
                    _loadedItems.Add(
                        item.PublishFieldId,
                        new LoadedItem
                        {
                            ItemsThatDependOnThis = list
                        });
                }
                else
                {
                    if (!record.ItemsThatDependOnThis.Contains(itemReasonId))
                    {
                        record.ItemsThatDependOnThis.Add(itemReasonId);
                    }
                }
                
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError($"Failed to load '{item.Title}'");
                Debug.LogError(e);
                return false;
            }
        }

        private Assembly LoadAssembly(SteamItem item)
        {
            var modPath = GetSteamItemDllPath(item);
            return CheckIfAssemblyIsLoaded(modPath, out var assembly) ? assembly : Assembly.LoadFrom(modPath);
        }

        private string GetSteamItemDllPath(SteamItem item) => Path.Combine(item.Directory, item.MetaData.DllName);

        private bool CheckIfAssemblyIsLoaded(string fullPath, out Assembly assembly)
        {
            var currentDomain = AppDomain.CurrentDomain;
            var currentAssemblies = currentDomain.GetAssemblies();

            assembly = currentAssemblies.FirstOrDefault(assembly => assembly.Location.Equals(fullPath));

            return assembly != null;
        }

        private bool ContainsVtolMod(Assembly assembly, out IEnumerable<Type> types)
        {
            types = from t in assembly.GetTypes()
                where t.IsSubclassOf(typeof(VtolMod))
                select t;

            return types.Any();
        }

        private void SpawnVtolMod(Type itemType, Assembly assembly, SteamItem item)
        {
            var id = (ItemId)Attribute.GetCustomAttribute(itemType, typeof(ItemId)) ??
                     throw new ItemIdMissingException($"Missing '{nameof(ItemId)}' attribute on '{itemType.Name}'");

            if (!HarmonyLib.Harmony.HasAnyPatches(id.UniqueValue))
            {
                var patch = new HarmonyLib.Harmony(id.UniqueValue);
                patch.PatchAll(assembly);
                Debug.Log($"Harmony Patched Assembly '{assembly.FullName}' with id of '{id.UniqueValue}'");
            }
            else
            {
                Debug.Log($"Assembly '{assembly.FullName}' was already patched!");
            }
            
            

            var itemGameObject = new GameObject(itemType.FullName, itemType);
            DontDestroyOnLoad(itemGameObject);

            Debug.Log($"Created GameObject '{itemGameObject.name}' from type '{itemType.Name}'");
        }

        [CanBeNull]
        private Assembly GetSteamItemAssembly(SteamItem item)
        {
            var path = GetSteamItemDllPath(item);

            if (!CheckIfAssemblyIsLoaded(path, out var assembly))
            {
                Debug.LogError("Attempted to unload");
                return null;
            }

            return assembly;
        }
    }
}
