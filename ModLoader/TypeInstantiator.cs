﻿using System;
using System.Collections.Generic;
using ModLoader.Assets;
using ModLoader.Assets.ReadyRoom;
using ModLoader.IntegratedMods;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ModLoader
{
    internal class TypeInstantiator : MonoBehaviour
    {
        private readonly Dictionary<VTOLScenes, Type[]> _types = new()
        {
            { VTOLScenes.SplashScene, new []{ typeof(AssetBundleLoader), typeof(SteamQuery.SteamQueries), typeof(ModLoader) }},
            { VTOLScenes.SamplerScene, new []{ typeof(DisableModLoaderButton), typeof(StartGameButtonPresser) }},
            { VTOLScenes.ReadyRoom , new []{  typeof(ModsPage), typeof(MouseVRInteract) }}
        };
        
        private void Awake()
        {
            Debug.Log("Hello From Type Instantiator");
            SceneManager.sceneLoaded += OnSceneLoaded;
            // This object gets created on the splash scene, so the event does 
            // not get fired. So we manually trigger it.
            OnSceneLoaded(VTOLScenes.SplashScene);
            DontDestroyOnLoad(this);
        }

        private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            var scene = (VTOLScenes)arg0.buildIndex;
            OnSceneLoaded(scene);
        }

        private void OnSceneLoaded(VTOLScenes scene)
        {
            if (!_types.TryGetValue(scene, out var objectsToSpawn))
            {
                return;
            }

            foreach (var monoBehaviour in objectsToSpawn)
            {
                new GameObject(monoBehaviour.Name, monoBehaviour);
                Debug.Log($"Created Type '{monoBehaviour.FullName}'");
            } 
        }

        private void OnDestroy()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }
}