﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Serilog;
using SimpleTCP;
using SteamQueries.Models;
using Steamworks;
using Steamworks.Ugc;

namespace SteamQueries;

internal class SteamQuery
{
    private readonly int _port;
    private readonly uint _appId;
    private bool _initialised;
    private SimpleTcpServer _tcpServer;
    
    public SteamQuery(int port, uint appId)
    {
        _port = port;
        // _appId = appId;
        _appId = 480;
    }

    public async Task<int> Init()
    {
        if (_initialised)
        {
            Log.Warning("Steam Query has already been initialised on port {Port} for app id {AppId}", _port, _appId);
            return -1;
        }

        try
        {
            _tcpServer = new SimpleTcpServer();
            _tcpServer.DataReceived += ((sender, message) => Task.Run(async delegate
            {
                await OnMessageEvent(sender, message);
            }));
            _tcpServer.Start(_port);
        }
        catch (Exception e)
        {
            Log.Error("Failed to create TCP Server on port {Port}. {ExceptionMessage}", _port, e.Message);
            return -1;
        }
        
        Log.Information("Connected to game on port {Port}", _port);
        

        try
        {
            SteamClient.Init(_appId);
            _initialised = true;
        }
        catch (Exception e)
        {
            Log.Error("Error when starting steamworks:\n{Error}", e.ToString());
            return -1;
        }
        
        Log.Information("Started Steamworks with app id of {AppId}", _appId);
        return 0;
    }

    private async Task OnMessageEvent(object sender, Message e)
    {
        var messageChopped = e.MessageString.Remove(e.MessageString.Length - 1);
        Log.Information("Raw message received '{Message}'", messageChopped);
        var message = default(IMessage);
        try
        {
            message = JsonConvert.DeserializeObject<IMessage>(messageChopped);
        }
        catch (Exception exception)
        {
            Log.Error(exception, "Error when trying to read message");
            return;
        }

        try
        {
            switch (message)
            {
                case GetSubscribedItemsRequest request:
                    await GetSubscribedItems(request, e);
                    break;
                case GetItemRequest request:
                    await GetItem(request, e);
                    break;
                default:
                    Log.Warning("The type {Type} wasn't handled for", message.GetType().FullName);
                    break;
            }
        }
        catch (Exception exception)
        {
            Log.Fatal(exception, "FAILED TO PROCESS REQUEST!");
        }
    }

    public async Task Tick()
    {
        var delay = TimeSpan.FromSeconds(1);
        while (_tcpServer.IsStarted)
        {
            await Task.Delay(delay);
        }
        Log.Information("Shutting down");
        SteamClient.Shutdown();
    }

    private async Task GetSubscribedItems(GetSubscribedItemsRequest request, Message message)
    {
        Log.Information("Received request to get user's subscribed items on page {Page}", request.Page);
        var query = Query.Items.WhereUserSubscribed().WithLongDescription(true).WithMetadata(true).WithChildren(true);
        var results = await query.GetPageAsync(request.Page);
        var response = new GetSubscribedItemsResponse
        {
            HasValues = results.HasValue,
            Items = results.Value.Entries.ToModel()
        };

        var responseText = JsonConvert.SerializeObject(response, Formatting.None,
            new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        
        Log.Information("Sending response for {MethodName}", nameof(GetSubscribedItems));
        message.Reply(responseText);
    }

    private async Task GetItem(GetItemRequest request, Message message)
    {
        Log.Information("Received request to get item details for {PublishFieldId}", request.PublishFieldId);
        var results = await Query.Items.WithFileId(request.PublishFieldId).WithLongDescription(true).WithMetadata(true).GetPageAsync(1);
        var response = new GetItemResponse
        {
            HasValues = results.HasValue,
            Items = results.Value.Entries.ToModel()
        };
        
        var responseText = JsonConvert.SerializeObject(response, Formatting.None,
            new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        
        Log.Information("Sending response for {MethodName}", nameof(GetItem));
        message.Reply(responseText);
    }
    
    
}