﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommandLine;
using Serilog;

namespace SteamQueries;

internal static class Program
{
    public static async Task Main(string[] args)
    {
        CreateLogger();
        Log.Information("Program started with args {Args}", args.Print());

        var parser = Parser.Default.ParseArguments<CommandLineOptions>(args);
        await parser.WithParsedAsync(ParsedArguments);
        await parser.WithNotParsedAsync(ErroredPassingArguments);
    }

    private static void CreateLogger()
    {
        var currentTime = DateTime.Now;
        var logPath = _logsDirectoryName + $"/Log_{currentTime.Ticks}.txt";
            
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Console()
            .WriteTo.File(logPath)
            .CreateLogger();
    }

    private static async Task ParsedArguments(CommandLineOptions options)
    {
        var query = new SteamQuery(options.Port, (uint)options.AppId);
        var startupResult = await query.Init();
        if (startupResult == 0)
        {
            await query.Tick();
        }
    }

    private static async Task ErroredPassingArguments(IEnumerable<Error> errors)
    {
        Log.Error("Errored parsing command line arguments");
        foreach (var error in errors)
        {
            Log.Information(error.Tag.ToString());
        }
    }

    private const string _logsDirectoryName = "Logs";
}